# Acknowledgment

## automatic install

```consol
archinstall
```

## clean install

- `first.sh` and `pacman.conf.patch`  
  [nomuken](https://sh.spica.bz/ins/archlinux/first.sh)

- `chroot.sh`  
  [既存の linux 環境から archlinux のインストール準備](http://piacco.github.io/2015/05/13/archlinux%E3%81%AE%E3%82%A4%E3%83%B3%E3%82%B9%E3%83%88%E3%83%BC%E3%83%AB/)

- `os_install.sh`  
  [年始に Linux デスクトップを使い始めて 1 年が経とうとしている](https://masawada.hatenablog.jp/entry/2019/12/02/000000)

## provisioning

## lists

[List of installed packages](https://wiki.archlinux.org/index.php/Pacman/Tips_and_tricks#List_of_installed_packages)

```consol
sudo pacman -Qqetn > pkglist.txt
sudo pacman -Qqem > foreignpkglist.txt
```

## make

[Dotfiles でバックアップ不要な開発環境](https://solist.work/blog/posts/dotfiles/)

## ansible

[pigmonkey/spark](https://github.com/pigmonkey/spark)
