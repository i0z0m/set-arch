export PATH := ${HOME}/.local/bin:${HOME}/.cargo/bin:/usr/local/bin:/usr/local/sbin:/usr/bin:/usr/bin/core_perl

rclone: ## Init rclone
	sudo pacman -S rclone
	chmod 600 ${PWD}/.config/rclone/rclone.conf
	test -L ${HOME}/.config/rclone || rm -rf ${HOME}/.config/rclone
	ln -vsfn ${PWD}/.config/rclone ${HOME}/.config/rclone

gnupg: ## Deploy gnupg (Run after rclone)
	sudo pacman -S git-crypt gnupg
	mkdir -p ${HOME}/.gnupg
	ln -vsf ${PWD}/.gnupg/gpg-agent.conf ${HOME}/.gnupg/gpg-agent.conf

ssh: ## Init ssh
	sudo pacman -S openssh
	mkdir -p ${HOME}/.ssh
	ln -vsf ${PWD}/.ssh/config ${HOME}/.ssh/config
	ln -vsf ${PWD}/.ssh/known_hosts ${HOME}/.ssh/known_hosts
	chmod 600 ${HOME}/.ssh/id_rsa

init: ## Initial deploy dotfiles
	test -L ${HOME}/.emacs.d || rm -rf ${HOME}/.emacs.d
	ln -vsfn ${PWD}/.emacs.d ${HOME}/.emacs.d
	ln -vsf ${PWD}/.lesskey ${HOME}/.lesskey
	lesskey
	ln -vsf ${PWD}/.zshrc ${HOME}/.zshrc
	ln -vsf ${PWD}/.vimrc ${HOME}/.vimrc
	ln -vsf ${PWD}/.bashrc ${HOME}/.bashrc
	ln -vsf ${PWD}/.tmux.conf ${HOME}/.tmux.conf
	ln -vsf ${PWD}/.aspell.conf ${HOME}/.aspell.conf
	ln -vsf ${PWD}/.gitconfig ${HOME}/.gitconfig
	mkdir -p ${HOME}/.config/mpv
	ln -vsf ${PWD}/.config/mpv/mpv.conf ${HOME}/.config/mpv/mpv.conf
	ln -vsf ${PWD}/.netrc ${HOME}/.netrc
	ln -vsf ${PWD}/.authinfo ${HOME}/.authinfo
	ln -vsf ${PWD}/.config/hub ${HOME}/.config/hub

base: ## Install base and base-devel packages using pkglist.txt
	@echo "Installing base packages from pkglist.txt..."
	xargs -a pkglist.txt sudo pacman -S --needed --noconfirm

install: ## Install AUR packages from foreignpkglist.txt
	@echo "Installing foreign packages from foreignpkglist.txt..."
	xargs -a foreignpkglist.txt yay -S --needed --noconfirm

pipinstall: ## Install python packages
	mkdir -p ${HOME}/.local
	curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py
	python ${PWD}/get-pip.py --user
	sudo ln -vsf ${PWD}/usr/share/zsh/site-functions/_pipenv /usr/share/zsh/site-functions/_pipenv
	pip install --user --upgrade pip
	pip install --user ansible
	pip install --user ansible-lint
	pip install --user autopep8
	pip install --user black
	pip install --user cheat
	pip install --user chromedriver-binary
	pip install --user diagrams
	pip install --user faker
	pip install --user flake8
	pip install --user gif-for-cli
	pip install --user graph-cli
	pip install --user httpie
	pip install --user importmagic
	pip install --user jupyter
	pip install --user jupyterlab
	pip install --user jupyterthemes
	pip install --user litecli
	pip install --user matplotlib
	pip install --user nose
	pip install --user progressbar2
	pip install --user psycopg2-binary
	pip install --user py-spy
	pip install --user pydoc_utils
	pip install --user pyflakes
	pip install --user pylint
	pip install --user 'python-language-server[all]'
	pip install --user requests_mock
	pip install --user rope
	pip install --user rtv
	pip install --user scipy
	pip install --user scrapy
	pip install --user seaborn
	pip install --user selenium
	pip install --user speedtest-cli
	pip install --user streamlink
	pip install --user tldr
	pip install --user trash-cli
	pip install --user truffleHog
	pip install --user virtualenv
	pip install --user virtualenvwrapper
	pip install --user yapf
	pip install --user sphinx
	pip install --user sphinx-ltd_theme
	pip install --user ghp-import==0.5.5
	pip install --user pynvim
	pip install --user meson
	pip install --user ninja
	rm -fr get-pip.py

rustinstall: ## Install rust and rust language server
	sudo pacman -S rustup
	rustup default stable
	rustup component add rls rust-analysis rust-src

alacritty: ## Init alacritty terminal
	sudo pacman -S alacritty

thinkpad: ## Workaround for Intel throttling issues in Linux
	sudo pacman -S throttled
	sudo systemctl enable --now lenovo_fix.service

keyring: ## Init gnome keyrings
	sudo pacman -S seahorse
	mkdir -p ${HOME}/.local/share
	test -L ${HOME}/.local/share/keyrings || rm -rf ${HOME}/.local/share/keyrings
	ln -vsfn ${HOME}/backup/keyrings ${HOME}/.local/share/keyrings

localhostssl: # Set ssl for localhost
	mkcert -install
	mkcert localhost

docker: ## Docker initial setup
	sudo pacman -S docker
	sudo usermod -aG docker ${USER}
	mkdir -p ${HOME}/.docker
	ln -vsf ${PWD}/.docker/config.json ${HOME}/.docker/config.json
	sudo systemctl enable docker.service
	sudo systemctl start docker.service

yay: ## Install AUR helper
	pacman -S --needed git base-devel
	git clone https://aur.archlinux.org/yay.git
	cd yay
	makepkg -si

docker-compose: ## Set up docker-compose
	sudo pacman -S docker-compose
	gcloud components install docker-credential-gcr

desktop: ## Update desktop entry
	sudo ln -vsf ${PWD}/usr/share/applications/vim.desktop /usr/share/applications/vim.desktop
	sudo ln -vsf ${PWD}/usr/share/applications/xterm.desktop /usr/share/applications/xterm.desktop
	sudo ln -vsf ${PWD}/usr/share/applications/uxterm.desktop /usr/share/applications/uxterm.desktop
	sudo ln -vsf ${PWD}/usr/share/applications/urxvt.desktop /usr/share/applications/urxvt.desktop
	sudo ln -vsf ${PWD}/usr/share/applications/urxvtc.desktop /usr/share/applications/urxvtc.desktop
	sudo ln -vsf ${PWD}/usr/share/applications/urxvt-tabbed.desktop /usr/share/applications/urxvt-tabbed.desktop

backup: ## Backup arch linux packages
	mkdir -p ${PWD}/archlinux
	pacman -Qnq > ${PWD}/archlinux/pacmanlist
	pacman -Qqem > ${PWD}/archlinux/aurlist

update: ## Update arch linux packages and save packages cache 3 generations
	yay -Syu; paccache -ruk0

pipbackup: ## Backup python packages
	mkdir -p ${PWD}/archlinux
	pip freeze > ${PWD}/archlinux/requirements.txt

piprecover: ## Recover python packages
	mkdir -p ${PWD}/archlinux
	pip install --user -r ${PWD}/archlinux/requirements.txt

pipupdate: ## Update python packages
	pip list --user | cut -d" " -f 1 | tail -n +3 | xargs pip install -U --user

rustupdate: ## Update rust packages
	cargo install-update -a

testbackup: ## Test this Makefile with mount backup directory
	docker build -t dotfiles ${PWD}
	docker run -it --name maketestbackup -v /home/${USER}/backup:${HOME}/backup:cached --name makefiletest -d dotfiles:latest /bin/bash;\
	docker exec -it maketestbackup sh -c "cd ${PWD}; make install";\
	docker exec -it maketestbackup sh -c "cd ${PWD}; make init";\
	docker exec -it maketestbackup sh -c "cd ${PWD}; make aur";\
	docker exec -it maketestbackup sh -c "cd ${PWD}; make pipinstall";\

test: ## Test this Makefile with docker without backup directory
	docker build -t dotfiles ${PWD};\
	docker run -it --name maketest -d dotfiles:latest /bin/bash;\
	docker exec -it maketest sh -c "cd ${PWD}; make install";\
	docker exec -it maketest sh -c "cd ${PWD}; make init";\
	docker exec -it maketest sh -c "cd ${PWD}; make aur";\
	docker exec -it maketest sh -c "cd ${PWD}; make pipinstall";\

testpath: ## Echo PATH
	PATH=$$PATH
	@echo $$PATH

allinstall: rclone gnupg ssh install init keyring urxvt xterm yay tlp thinkpad ttf-cica dnsmasq pipinstall docker zeal sylpheed lvfs docker-compose toggle aur kind eralchemy mpsyt

nextinstall: chromium rustinstall

allupdate: update pipupdate rustupdate

allbackup: backup pipbackup

.PHONY: allinstall nextinstall allupdate allbackup

help:
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) \
	| sort \
	| awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

.DEFAULT_GOAL := help
