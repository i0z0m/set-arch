FROM archlinux:base

LABEL maintainer="i0z0mu@gmail.com"

ARG USER
ARG PASSWD

RUN useradd -m $USER && \
    echo "$USER:$PASSWD" | chpasswd && \
    echo "$USER ALL=(ALL) ALL" >> /etc/sudoers

RUN pacman -Syu --noconfirm && \
    pacman -S --noconfirm \
    sudo \
    make \
    git \
    which \
    python \
    python-pip \
    vim \
    yarn \
    zsh \
    tmux \
    fzf \
    fd \
    bat \
    ripgrep \
    thefuck

RUN yay -S emacs-lucid --noconfirm
